const { WebhookClient } = require('dialogflow-fulfillment');
const dateFormat = require('dateformat');
const Promise = require('bluebird');
const player = require('play-sound')({
  player: 'mplayer',
});

const logger = require('../logger');

let currentMusic;

async function startMusic() {
  return Promise.try(() => {
    currentMusic = player.play('./music/mozart.mp3', err => {
      if (err && !err.killed) logger.err(err);
      Promise.resolve();
    });
  });
}

async function stopMusic() {
  return Promise.try(() => currentMusic.kill()).catch(err => logger.err(err));
}

module.exports = app => async (req, res) => { // eslint-disable-line no-unused-vars, prettier/prettier
  const webhookClient = await Promise.try(
    () => new WebhookClient({ request: req, response: res }),
  );

  if (!webhookClient) {
    logger.warn('Abort the request %s', req.url);
    return;
  }

  logger.debug('Req locale: %s', webhookClient.locale);
  logger.debug(
    'Req details: %s, %s',
    webhookClient.context.session,
    JSON.stringify(webhookClient.parameters),
  );

  async function intentDefaultWelcomeAskName(agent) {
    const { firstname, lastname } = agent.parameters;
    agent.add(`Merci ${firstname} ${lastname}`);
  }

  async function intentObjectAction(agent) {
    const { object, objectAction, time } = agent.parameters;
    agent.add(
      time
        ? `C'est noté, ${objectAction} pour ${object} à ${dateFormat(
          new Date(time), // eslint-disable-line prettier/prettier
          'UTC:h:MM:ss TT  Z', // eslint-disable-line prettier/prettier
        )} :)` // eslint-disable-line prettier/prettier
        : `C'est noté, ${objectAction} pour ${object} :)`,
    );
  }

  async function intentMusicActionDefault(agent) {
    const { musicAction } = agent.parameters;
    agent.add("C'est comme si c'était fait.");

    if (musicAction === 'turn on') startMusic();
    else stopMusic();
  }

  async function intentMusicActionArtist(agent) {
    const { musicAction, musicArtist } = agent.parameters;
    agent.add(`C'est noté ${musicAction} de ${musicArtist}.`);

    if (musicAction === 'turn on') startMusic();
    else stopMusic();
  }

  async function intentMusicActionGenre(agent) {
    const { musicAction, musicGenre } = agent.parameters;
    agent.add(`C'est noté ${musicAction} de ${musicGenre}.`);

    if (musicAction === 'turn on') startMusic();
    else stopMusic();
  }

  const intentMap = new Map();
  intentMap.set('defaultWelcomeThenAskName', intentDefaultWelcomeAskName);
  intentMap.set('objectAction', intentObjectAction);
  intentMap.set('musicActionDefault', intentMusicActionDefault);
  intentMap.set('musicActionGenre', intentMusicActionGenre);
  intentMap.set('musicActionArtist', intentMusicActionArtist);

  try {
    await webhookClient.handleRequest(intentMap);
  } catch (err) {
    if (err.message === 'No handler for requested intent') {
      logger.warn(
        'Unkown action "%s", %s',
        webhookClient.intent,
        webhookClient.context.session,
      );
    } else {
      logger.err(err);
    }
  }
};
