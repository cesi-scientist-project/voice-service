require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');

const argv = require('./argv');
const logger = require('./logger');
const router = require('./router');

const serverPort = parseInt(argv.port || process.env.PORT || '3000', 10);
const serverHostCustom = argv.host || process.env.HOST;
const serverHost = serverHostCustom || null;
const serverHostPretty = serverHostCustom || 'localhost';

const app = express();

app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);
app.use(bodyParser.json());
app.use(methodOverride());

router(app);

app.listen(serverPort, serverHost, async err => {
  if (err) {
    return logger.err(err.message);
  }
  logger.appStarted(serverPort, serverHostPretty);
  return 'ok';
});
