const routeDialogflow = require('./routes/dialogflow');
const logger = require('./logger');

module.exports = app => {
  app.use((err, req, res, next) => {
    logger.err(err);
    next(err);
  });

  app.use((err, req, res, next) => {
    if (req.xhr) {
      res.status(500).send({ error: 'Something failed!' });
    } else {
      next(err);
    }
  });

  app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars, prettier/prettier
    logger.warn(`Error 500 from ${req.url}`);

    res.status(500);
    res.send(`<h1>500</h1><p>Internal server error:</p><p>${err.message}</p>`);
  });

  app.use('/dialogflow', routeDialogflow(app));

  app.use((req, res) => { // eslint-disable-line no-unused-vars, prettier/prettier
    logger.warn(`Error 404 from ${req.url}`);

    res.status(404).send('<h1>404</h1><p>Page not found</p>');
  });
};
